You find yourself in an odd situation. You are a hound and your master just shoot down your next meal. This is a pretty standard situation except for the fact that you are trapped in a maze. Use your nose to find the prey and come back to your master with a map showing how to get there.  

You have a map in a TSV format where the F represents spaces to walk on and the blanks represent walls. Create a program that takes you to the point in the map where the prey is located. (Your initial position and the final position are given in the zip with the map). Provide a solution as an array of arrays with positions [X,Y] from point 0 to point N. There are some smaller sample files in the ZIP so you can test your solution.  ZIP File http://raffle.wearegap.com/maps.zip - The password is: AgileIsInOurDNA

Once you have created the program, send us an email to codehunt@wearegap.com; Subject “Solution - Your Name”. Feel free to add details about yourself: Name, location. Also, tell us how you want to develop your professional career.  Most importantly, add a JSON file with the following information Email, Repo (url of the repo) and solution path.

{
	"email": "your_email",
	"repo": "URL to git repo with your solution",
	"solution": [[0, 0],[1, 0],[2, 1],[3, 3]]
}
