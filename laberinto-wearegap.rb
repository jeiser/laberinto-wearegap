@arr = Array.new(1) { "" }                                                      #Declaración del array del mapa. Para compensar la coordenada '0', se carga la primera fila vacía
@flag = false                                                                   #Flag para punto de control de los hilos

puts "Por favor ingrese el fileName y su extensión (Ej: Sample1.tsv ) --> "
fileName = gets.chomp                                                           #Nombre exacto del filename y su extención
puts "Por favor ingrese el startpoint (Ej: [31,6] ) --> "
@coor_ini = gets.scan(/\w+/)                                                    #Coordenada del StartPoint
puts "Por favor ingrese el endpoint (Ej: [19,17] ) --> "
@coor_fin = gets.scan(/\w+/)                                                    #Coordenada del EndPoint

File.open(fileName, 'r') do |fn|                                                #Carga del mapa
    while linea = fn.gets
        @arr.push [""].concat(linea.split(?\t).map(&:to_s)).concat([""])        #Para compensar la coordenada '0', se carga la primera y última posición vacía
    end
    @arr.push [""]                                                              #Para compensar limite del mapa, se carga ultima fila vacía
end

def recorrerMapa f, c, cola                                                     #Función recursiva que recorre el mapa
    unless cola.include? "[#{c},#{f}]" or @flag                                 #Verificación de coordenada ya guardada. Control de hilos.
        if (f == @coor_fin[1].to_i and c == @coor_fin[0].to_i)                  #Verificación de coordenada final
            @flag = true                                                        #Cambio flag
            cola.push "[#{c},#{f}]"                                             #última inserción
            puts "\nResultado: \n"+cola.inspect                                 #Impresión de resultado
            exit                                                                #Salida del programa
        else
            if (@arr[f][c].to_s.chomp == "F")                                   #Verificación de camino
                cola.push "[#{c},#{f}]"                                         #Inserción de coordenada
                [Thread.new {recorrerMapa f+1, c, cola.dup},                    #Declaración de hilos para búsqueda recursiva
                Thread.new {recorrerMapa f, c-1, cola.dup},                     #Se envían instancias nuevas de colas para evitar solapación por scope
                Thread.new {recorrerMapa f-1, c, cola.dup},                     #Se envían los 4 puntos posibles siguientes para evaluación
                Thread.new {recorrerMapa f, c+1, cola.dup}]
                .map(&:join)                                                    #Lanzamiento de los hilos
            end
        end
    end
    Thread.current.kill                                                         #Liberación de hilos sueltos
end

recorrerMapa @coor_ini[1].to_i, @coor_ini[0].to_i, []                           #Lanzamiento de la función de búsqueda
